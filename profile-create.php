<?php
    $nouveauCookie = '';

    // On déclare un tableau d'erreur vide
    $errors = [];
    // On vérifie qu'on est sur une méthode de type post
    if($_SERVER["REQUEST_METHOD"] == 'POST'){
        // On stock notre username si il n'est pas vide
        if(!empty($_POST["username"])){
            // On le stock dans un cookie
            setcookie("username", $_POST["username"]);
            $nouveauCookie = $_POST["username"];
        } else{
            // Si non on lui demande de saisir un username
            $errors[] = "Veuillez sais un username";
        }

        // On déclare une variable qui est égale à la taille max en octet d'un fichier
        // Pour que personne ne sature le serveur avec des fichiers lourd
        $maxSize = 2000000;

        // On déclare un tableau avec les mimetype autorisé pour l'uploads
        // On pourra donc uploader des jpeg, des png et des giff
        $allowedFormat = ["image/jpeg", 'image/png', 'image/gif'];


        // On vérifie le retour erreur de php si il est égal à 0 c'est coo
        if(!$_FILES["profile_picture"] || $_FILES["profile_picture"]["error"] != 0){

            if(!isset($_COOKIE["profile_path"])){
                $errors[] = 'Une erreur inconnue vient d\'arriver';
            }

            // On vérifie que le type du fichier uploadé est autorisé dans le tableau ci-dessus
        } else if(!in_array($_FILES["profile_picture"]["type"], $allowedFormat)){
            $errors[] = 'Mauvais type de fichier';
            // Check sur la taille du fichier
        } else if( $_FILES["profile_picture"]['size'] > $maxSize){
            $errors[] = 'Le fichier est trop lourd';
        }





        // Si jamais nous n'avons pas d'erreur on ira uploader notre fichier
        if(count($errors) == 0 && $_FILES["profile_picture"]["size"]!=0){

            $extension = $_FILES["profile_picture"]["type"];
            $extension = explode("/", $extension)[1];
            $name = uniqid().'.'.$extension;


            move_uploaded_file($_FILES["profile_picture"]["tmp_name"], "uploads/".$name);

            setcookie("profile_path", $name);


            header("Location: profile-view.php");


        } elseif( $_FILES["profile_picture"]["size"]==0){
            header("Location: profile-view.php");
        }

    }
?>
<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';
    ?>
</head>
<body>

<div class="container">

    <?php
        include "parts/menu.php";
    ?>


    <h1>Création d'un profil !</h1>


    <form method="post" action="profile-create.php" enctype="multipart/form-data">
        <div class="form-group row mt-5">
            <label for="inputPassword" class="col-sm-2 col-form-label">Username</label>
            <div class="col-sm-10">
                <input <?php
                    if(isset($_COOKIE["username"])){
                        echo('value="'.$_COOKIE["username"].'"');
                    }
                 ?>type="text" name="username" class="form-control" id="inputPassword" placeholder="Saisissez votre nom d'utilisateur">
            </div>
        </div>

        <div class="form-group row mt-5">
            <label for="inputPassword" class="col-sm-2 col-form-label">Avatar !</label>
            <div class="col-sm-10">
                <input type="file" name="profile_picture" class="form-control" id="inputPassword" placeholder="Saisissez votre nom d'utilisateur">
            </div>

            <?php
                if(isset($_COOKIE["profile_path"])){
                    echo('Votre image actuelle : Si vous en uploadez une autre elle sera modifié sinon conservé !<br>');
                    echo(' <img style="max-width: 500px;" class="img-thumbnail mt-5" alt="Ma photo de profil !" src="uploads/'. $_COOKIE["profile_path"] .'">');
                }
            ?>
        </div>

        <input type="submit" class="btn btn-success mt-5">
    </form>

    <?php
        // Ici on affiche le tableau avec toutes nos erreurs !
        foreach ($errors as $error){
            echo('<div class="error alert-danger">'.$error.'</div>');
        }
    ?>


</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>
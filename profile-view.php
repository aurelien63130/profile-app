<?php
    if(!isset($_COOKIE["username"]) || !isset($_COOKIE["profile_path"])){
        header("Location: profile-create.php");
    }
?>
<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';
    ?>
</head>
<body>

<div class="container">

    <?php
    include "parts/menu.php";
    ?>


    <h1>Bonjour <?php echo($_COOKIE["username"]); ?> !</h1>


    <img alt="Ma photo de profil !" src="uploads/<?php echo($_COOKIE["profile_path"]);?>">

    <a href="profile-create.php">Modifier !</a>

</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>